#include <iostream>
#include <boost/range/irange.hpp>
#include <string>
using namespace std;

void conv( char *s ){
	auto x = stof(s);
	cout<<x*x<<endl;
}

int main( int argc, char** argv){

	for ( int i : boost::irange(1,argc)){
		conv(argv[i]);
	}

}